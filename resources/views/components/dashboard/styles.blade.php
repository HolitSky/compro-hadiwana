<!-- App favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('hdw-icon.png') }}">
<!-- Bootstrap Css -->
<link href="{{ asset('Assets/Dashboard/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{ asset('Assets/Dashboard/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="{{ asset('Assets/Dashboard/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
<!-- Sweet alert-->
<link href="{{ asset('Assets/Dashboard/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
<!-- custom-->
<link href="{{ asset('Assets/Dashboard/css/custom.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
