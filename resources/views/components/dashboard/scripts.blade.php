<!-- JAVASCRIPT -->
<script src="{{ asset('Assets/Dashboard/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/node-waves/waves.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<!--Morris Chart-->
<script src="{{ asset('Assets/Dashboard/libs/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/libs/raphael/raphael.min.js') }}"></script>


{{-- <script src="{{ asset('Assets/Dashboard/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js') }}"></script> --}}
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="{{ asset('Assets/Dashboard/js/pages/dashboard.init.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/js/app.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/js/togglePass.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/js/lengthChar.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/js/previewImage.js') }}"></script>
<script src="{{ asset('Assets/Dashboard/js/pages/sweet-alerts.init.js') }}"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

<!--table-->

<script>
    $(document).ready(function () {
        $('#info-content-toggle').on('click', function () {
            $('.content-toggle').toggle();
        });
    });
</script>





