<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © 2024 Hadiwana Tirta Lestari | Dashboard <span class="d-none d-sm-inline-block">- by K.S.</span>
            </div>
        </div>
    </div>
</footer>