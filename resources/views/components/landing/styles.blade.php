<!-- HADIWANA ICON-->
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('hdw-icon.png') }}">
<!-- Google Fonts Css-->
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:ital,wght@0,200..800;1,200..800&amp;display=swap" rel="stylesheet">
<!-- Bootstrap Css -->
<link href="{{ asset('Assets/Landing/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
<!-- SlickNav Css -->
<link href="{{ asset('Assets/Landing/css/slicknav.min.css') }}" rel="stylesheet">
<!-- Swiper Css -->
<link rel="stylesheet" href="{{ asset('Assets/Landing/css/swiper-bundle.min.css') }}">
<!-- Font Awesome Icon Css-->
<link href="{{ asset('Assets/Landing/css/all.css') }}" rel="stylesheet" media="screen">
<!-- Animated Css -->
<link href="{{ asset('Assets/Landing/css/animate.css') }}" rel="stylesheet">
<!-- Magnific Popup Core Css File -->
<link rel="stylesheet" href="{{ asset('Assets/Landing/css/magnific-popup.css') }}">
<!-- Mouse Cursor Css File -->
<link rel="stylesheet" href="{{ asset('Assets/Landing/css/mousecursor.css') }}">
<!-- Main Custom Css -->
<link href="{{ asset('Assets/Landing/css/custom.css') }}" rel="stylesheet" media="screen">

<style>
    .post-featured-image {
    aspect-ratio: 16 / 9;  /* Atur sesuai rasio yang diinginkan, misalnya 16:9 */
    overflow: hidden;
    }

    .card-image {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
</style>
