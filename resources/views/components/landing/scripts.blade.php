
    <!-- Jquery Library File -->
    <script src="{{ asset('Assets/Landing/js/jquery-3.7.1.min.js') }}"></script>
    <!-- Bootstrap js file -->
    <script src="{{ asset('Assets/Landing/js/bootstrap.min.js') }}"></script>
    <!-- Validator js file -->
    <script src="{{ asset('Assets/Landing/js/validator.min.js') }}"></script>
    <!-- SlickNav js file -->
    <script src="{{ asset('Assets/Landing/js/jquery.slicknav.js') }}"></script>
    <!-- Swiper js file -->
    <script src="{{ asset('Assets/Landing/js/swiper-bundle.min.js') }}"></script>
    <!-- Counter js file -->
    <script src="{{ asset('Assets/Landing/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('Assets/Landing/js/jquery.counterup.min.js') }}"></script>
    <!-- Isotop js file -->
	<script src="{{ asset('Assets/Landing/js/isotope.min.js') }}"></script>
    <!-- Magnific js file -->
    <script src="{{ asset('Assets/Landing/js/jquery.magnific-popup.min.js') }}"></script>
    <!-- SmoothScroll -->
    <script src="{{ asset('Assets/Landing/js/SmoothScroll.js') }}"></script>
    <!-- Parallax js -->
    <script src="{{ asset('Assets/Landing/js/parallaxie.js') }}"></script>
    <!-- MagicCursor js file -->
    <script src="{{ asset('Assets/Landing/js/gsap.min.js') }}"></script>
    <script src="{{ asset('Assets/Landing/js/magiccursor.js') }}"></script>
    <!-- Text Effect js file -->
    <script src="{{ asset('Assets/Landing/js/SplitText.js') }}"></script>
    <script src="{{ asset('Assets/Landing/js/ScrollTrigger.min.js') }}"></script>
    <!-- Wow js file -->
    <script src="{{ asset('Assets/Landing/js/wow.js') }}"></script>
    <!-- Main Custom js file -->
    <script src="{{ asset('Assets/Landing/js/function.js') }}"></script>

    
    