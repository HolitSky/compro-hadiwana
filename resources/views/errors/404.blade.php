<x-landing.layout>
    <style>

        .page_404 {
            text-align: center;
            width: 100%;
        }

        .four_zero_four_bg {
            background-image: url(https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif);
            height: 350px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
        }

        .four_zero_four_bg h1 {
            font-size: 80px;
            margin: 0;
            padding-top: 10px;
            color: #d32907;
        }

        .contant_box_404 {
            margin-bottom: 30px;
        }

        .contant_box_404 h3 {
            font-size: 40px;
            margin: 0 0 10px;
        }

        .contant_box_404 p {
            margin: 0 0 20px;
        }

        .link_404 {
            color: #fff;
            padding: 10px 20px;
            background: #39ac31;
            text-decoration: none;
            display: inline-block;
        }
    </style>
    <section class="page_404">
        <div class="four_zero_four_bg">
            <h1>Page Not Found</h1>
        </div>

        <div class="contant_box_404">
            <h3>Look like you're lost</h3>
            <p>The page you are looking for not available!</p>
        </div>
    </section>
</x-landing.layout>