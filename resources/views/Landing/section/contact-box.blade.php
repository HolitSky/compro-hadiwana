   <div class="cta-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cta-content">
                    <!-- Section Title Start -->
                    <div class="section-title">
                        <h3 class="wow fadeInUp">{{ __('general.contact_us') }}</h3>
                        <h2 class="text-anime-style-3" data-cursor="-opaque">{{ __('general.title_contact_section') }}</h2>
                    </div>
                    <!-- Section Title End -->

                    <!-- Cta Content Body Start -->
                    <div class="cta-content-body wow fadeInUp" data-wow-delay="0.25s">
                        <p>{{ __('general.desc_contact_section') }}</p>
                    </div>
                    <!-- Cta Content Body End -->

                    <!-- Cta Content Footer Start -->
                    <div class="cta-content-footer wow fadeInUp" data-wow-delay="0.5s">
                        <a href="{{ route('contact') }}" class="btn-default">{{ __('general.get_started') }}</a>
                    </div>
                    <!-- Cta Content Footer End -->
                </div>
            </div>
        </div>
    </div>
</div>