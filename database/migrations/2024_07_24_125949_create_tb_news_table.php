<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_news', function (Blueprint $table) {
            $table->id();
            $table->string('id_title');
            $table->string('id_description');
            $table->string('en_title');
            $table->string('en_description');
            $table->string('image')->nullable();
            $table->string('author');
            $table->date('news_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_news');
    }
};
