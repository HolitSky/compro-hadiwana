<?php
return [
    'useful_links' => 'Useful Links',
    'contact' => 'Contact',
    'description' => 'Committed to keeping you informed. We regularly share updates on our activities, news, and project results with our supporters and the public.',
    'about_us' => 'About Us',
    'services' => 'Services',
    'news' => 'News',
    'contact_us' => 'Contact Us',
    'activities' => 'Activities',
    'education' => 'Education',
    'projects' => 'Projects',
];