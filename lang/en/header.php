<?php
return [
    'home' => 'Home',
    'about' => 'About',
    'services' => 'Services',
    'news' => 'News',
    'activities' => 'Activities',
    'education' => 'Education',
    'projects' => 'Projects',
    'contact_us' => 'Contact Us',
];