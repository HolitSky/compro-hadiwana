<?php
return [
    // button
    'get_started' => 'Get Started',
    'view_more' => 'View More',
    'read_more' => 'Read More',
    'discover' => 'Discover',
    'learn_more' => 'Learn More',
    'view_services' => 'View Services',
    'view_all_services' => 'View All Services',
    'contact_us' => 'Contact Us',
    'contact' => 'Contact',
    'send_message' => 'Send Message',
    
    //title and desc
    'about_us' => 'About Us',
    'our_services' => 'Our Services',
    'best_conservation_planning_desc' => 'Best Conservation Planning and Implementation',
    'why_choose_us' => 'Why Choose Us',
    'our_projects' => 'Our Projects',
    'social_media' => 'Social Media',
    'global_present' => 'Global Present',
    'check_out_office' => 'Check Out Our Office Location',
    'how_can_we_help_you' => 'How can we help you today?',
    'let_us_know' => "Let us know who you are and what you're looking for below",
    'our_present' => "Our presence in this iconic city reflects our dedication to being innovation.",
    'everything_you_know_about_our_services' => "Everything you need to know about our services",
    'explore_our_latest_projects' => "Explore our latest projects",
    'desc_project_section' => "At Hadiwana, a passionate conservation organization dedicated to safeguarding Indonesia's rich biodiversity, proudly presents its latest initiative: the Wildlife Conservation Program. This comprehensive program aims to protect endangered species and foster harmonious coexistence between humans and wildlife.",
    'latest_post' => 'Latest Post',
    'read_our_latest_desc' => 'Read our latest conservation insights',

    'years_of_experience' => 'years of experience',
    'years_experience' => 'Years Experience',
    'projects_completed' => 'Projects Completed',
    'satisfied_client' => 'Satisfied Client',
    'overall_rating' => 'Overall Rating',
    'how_it_work' => 'How It Work',
    'meet_people' => 'Meet People',
    'meet_people_desc' => 'Get to Know the Experts',
    'proven_experience' => 'Proven Experience',
    'quality_services' => 'Quality Services',
    
    'faq_title' => 'Frequently Asked Questions',
    'all_services' => 'All Services',
    'all_services_desc' => 'Explore Our Services',
    'all_news' => 'All Services',
    'all_news_desc' => 'Discover Our News Updates',
    'all_activities' => 'All Activities',
    'all_activities_desc' => 'Discover Our Activities Updates',
    'all_education' => 'All Education',
    'all_education_desc' => 'Explore Our Education Updates',
    'all_projects' => 'All Projects',
    'all_projects_desc' => 'Explore Our Projects Updates',
    'all_videos' => 'All Videos',
    'all_videos_desc' => 'Explore Our Videos',

    //details
    'description' => 'Description',
    'category' => 'Category',

    'activities_details' => 'Activity Details',
    'other_activities' => 'Other Activities',
    'discover_more_activities_details' => 'Discover More of Our Activities Updates',

    'projects_details' => 'Project Details',
    'other_projects' => 'Other Projects',
    'discover_more_projects_details' => 'View our other project updates',

    'education_details' => 'Education Details',
    'other_education' => 'Other education',
    'discover_more_education_details' => 'Discover More of Our Education Updates',

    'news_details' => 'News Details',
    'other_news' => 'Other News',
    'discover_more_news_details' => 'Discover More of Our News Updates',

    'service_details' => 'Service Details',
    'feel_free_explore_service' => 'Feel free to explore our other services',

    'project_information' => 'Project Information',
    'project_name' => 'Project Name',
    'project_date' => 'Project Date',
    'client_company' => 'Client / Company',
    'project_location' => 'Project Location',
    'about_the_project' => 'About The Project',

    // contact section
    'title_contact_section' => "Ready to get started? Let's talk to us today",
    'desc_contact_section' => "We're eager to hear from you and explore how we can work together to safeguard our planet's precious biodiversity. Together, we can make a lasting impact on the future of our environment and the incredible creatures that inhabit it.",

    'captcha_placeholder' => 'Enter the CAPTCHA Code',

    //input form
    'your_name_label' => 'Your Name',
    'your_name_input' => 'Enter Full Name',

    'your_email_label' => 'Your Email',
    'your_email_input' => 'Enter Email',

    'your_phone_label' => 'Phone Number',
    'your_phone_input' => 'Enter Phone Number',

    'your_message_label' => 'Messages/Complaints/Suggestions',
    'your_message_input' => 'Enter Your Messages/Complaints/Suggestions',
];