<?php
return [
    'home' => 'Beranda',
    'about' => 'Tentang',
    'services' => 'Layanan',
    'news' => 'Berita',
    'activities' => 'Kegiatan',
    'education' => 'Edukasi',
    'projects' => 'Proyek',
    'contact_us' => 'Kontak Kami',
];