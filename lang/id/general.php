<?php
return [
    // button
    'get_started' => 'Mulai',
    'view_more' => 'Lihat Selengkapnya',
    'read_more' => 'Selengkapnya',
    'discover' => 'Jelajahi',
    'learn_more' => 'Selengkapnya',
    'view_services' => 'Lihat Layanan',
    'view_all_services' => 'Lihat Semua Layanan',
    'contact_us' => 'Kontak Kami',
    'contact' => 'Kontak',
    'send_message' => 'Kirim Pesan',

    //title and desc
    'about_us' => 'Tentang Kami',
    'our_services' => 'Layanan Kami',
    'best_conservation_planning_desc' => 'Perencanaan dan Implementasi Konservasi Terbaik',
    'why_choose_us' => 'Mengapa Memilih Kami',
    'our_projects' => 'Proyek Kami',
    'social_media' => 'Media Sosial',
    'global_present' => 'Kehadiran Global',
    'check_out_office' => 'Lihat Lokasi Kantor Kami',
    'how_can_we_help_you' => 'Bagaimana kami dapat membantu Anda hari ini?',
    'let_us_know' => 'Beritahu kami siapa Anda dan apa yang Anda cari di bawah ini',
    'our_present' => 'Kehadiran kami di kota ikonik ini mencerminkan dedikasi kami terhadap inovasi.',
    'everything_you_know_about_our_services' => 'Yang perlu anda ketahui tentang layanan kami',
    'explore_our_latest_projects' => 'Jelajahi proyek terbaru kami',
    'desc_project_section' => 'Hadiwana, sebuah organisasi konservasi yang berdedikasi untuk melindungi keanekaragaman hayati Indonesia yang kaya, dengan bangga mempersembahkan inisiatif terbarunya: Program Konservasi Satwa Liar. Program komprehensif ini bertujuan untuk melindungi spesies yang terancam punah dan memupuk keharmonisan antara manusia dan satwa liar.',
    'latest_post' => 'Berita Terbaru',
    'read_our_latest_desc' => 'Baca wawasan konservasi terbaru kami',

    'years_of_experience' => 'Tahun Pengalaman',
    'years_experience' => 'Pengalaman Bertahun-tahun',
    'projects_completed' => 'Proyek yang Selesai',
    'satisfied_client' => 'Klien yang Puas',
    'overall_rating' => 'Penilaian Keseluruhan',
    'how_it_work' => 'Cara Kerja Kami',
    'meet_people' => 'Kenali Ahlinya',
    'meet_people_desc' => 'Kenali Para Ahli',
    'proven_experience' => 'Pengalaman Terbukti',
    'quality_services' => 'Layanan Berkualitas',

    'faq_title' => 'Pertanyaan yang Sering Diajukan',
    'all_services' => 'Semua Layanan',
    'all_services_desc' => 'Jelajahi Layanan Kami',
    'all_news' => 'Semua Berita',
    'all_news_desc' => 'Temukan Pembaruan Berita Kami',
    'all_activities' => 'Semua Kegiatan',
    'all_activities_desc' => 'Temukan Pembaruan Kegiatan Kami',
    'all_education' => 'Semua Edukasi',
    'all_education_desc' => 'Jelajahi Pembaruan Edukasi Kami',
    'all_projects' => 'Semua Proyek',
    'all_projects_desc' => 'Jelajahi Pembaruan Proyek Kami',
    'all_videos' => 'Semua Video',
    'all_videos_desc' => 'Jelajahi Video Kami',

    //details
    'description' => 'Deskripsi',
    'category' => 'Kategori',

    'activities_details' => 'Detail Kegiatan',
    'other_activities' => 'Kegiatan Lainnya',
    'discover_more_activities_details' => 'Temukan Lebih Banyak Pembaruan Kegiatan Kami',

    'projects_details' => 'Detail Proyek',
    'other_projects' => 'Proyek Lainnya',
    'discover_more_projects_details' => 'Lihat Pembaruan Proyek Kami lainnya',

    'education_details' => 'Detail Edukasi',
    'other_education' => 'Edukasi Lainnya',
    'discover_more_education_details' => 'Temukan Lebih Banyak Pembaruan Edukasi Kami',

    'news_details' => 'Detail Berita',
    'other_news' => 'Berita Lainnya',
    'discover_more_news_details' => 'Temukan Lebih Banyak Pembaruan Berita Kami',

    'service_details' => 'Detail Layanan',
    'feel_free_explore_service' => 'Jangan ragu untuk menjelajahi layanan kami yang lain',

    'project_information' => 'Informasi Proyek',
    'project_name' => 'Nama Proyek',
    'project_date' => 'Tanggal Proyek',
    'client_company' => 'Klien / Perusahaan',
    'project_location' => 'Lokasi Proyek',
    'about_the_project' => 'Tentang Proyek',


    // contact section
    'title_contact_section' => "Siap untuk memulai? Mari bicara dengan kami hari ini",
    'desc_contact_section' => "Kami sangat bersemangat untuk mendengar dari Anda dan menjelajahi bagaimana kita dapat bekerja sama untuk melindungi keanekaragaman hayati yang berharga di planet kita Bersama-sama, kita dapat memberikan dampak yang berkelanjutan pada masa depan lingkungan kita dan makhluk-makhluk luar biasa yang menghuninya",

    'captcha_placeholder' => 'Masukan Kode CAPTCHA',

    //input form
    'your_name_label' => 'Nama Anda',
    'your_name_input' => 'Masukkan Nama Lengkap',

    'your_email_label' => 'Email Anda',
    'your_email_input' => 'Masukkan Email',

    'your_phone_label' => 'Nomor Telepon',
    'your_phone_input' => 'Masukkan Nomor Telepon',

    'your_message_label' => 'Pesan/Pengaduan/Saran',
    'your_message_input' => 'Masukkan Pesan/Pengaduan/Saran Anda',
];