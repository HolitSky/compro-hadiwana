<?php
return [
    'useful_links' => 'Tautan Berguna',
    'contact' => 'Kontak',
    'description' => 'Berkomitmen untuk membuat anda tetap terinformasi. Kami secara rutin membagikan pembaruan kegiatan, berita, dan hasil proyek kepada pendukung kami dan masyarakat umum.',
    'about_us' => 'Tentang Kami',
    'services' => 'Layanan',
    'news' => 'Berita',
    'contact_us' => 'Kontak Kami',
    'activities' => 'Kegiatan',
    'education' => 'Edukasi',
    'projects' => 'Proyek',

];